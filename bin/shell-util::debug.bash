#!/usr/bin/env bash

if [ -z "$BASH_VERSION" ]; then
	printf '%s\n' "$0: Not a bash shell"
	exit 1
fi

if [[ -z "$SH_UTIL_VERSION" ]]; then
	source ./shell-util::base.bash
fi

declare -A SH_UTIL_DEBUG_traps
sh-util::debug::traps(){
	case $1 in
	setup|add)
		local current_trap
		current_trap="$(trap -p DEBUG)"
		[[ $current_trap != *"${FUNCNAME[0]}"* ]] &&
			trap "${FUNCNAME[0]}"' run "$LINENO"; '"$current_trap" DEBUG
		SH_UTIL_DEBUG_traps[$2]="$3"
		;;
	run)
		local arg
		for fn in "${!SH_UTIL_DEBUG_traps[@]}"; do
			arg="${SH_UTIL_DEBUG_traps[$fn]}"
			"$fn" "$2" "${arg:+${!arg}}"
		done
	;;
	esac
}
sh-util::debug::vars(){
	case $1 in
	setup|add)
		if ! declare -p SH_UTIL_DEBUG_vars >&- 2>&-; then
			declare -A SH_UTIL_DEBUG_vars
			sh-util::debug::traps setup "${FUNCNAME[0]}" run
		fi
		shift
		# add names of variables you want to keep track of as keys
		for key in "$@"; do
			SH_UTIL_DEBUG_vars[$key]="${!key}"
		done
		;;
	run)
		local msg
		for key in "${!SH_UTIL_DEBUG_vars[@]}"; do
			if [[ "${SH_UTIL_DEBUG_vars[$key]}" != "${!key}" ]]; then
				printf -v msg '%4s: "$%s": <[ %s ]> => <[ %s ]>' \
					" L$1" "$key" "${SH_UTIL_DEBUG_vars[$key]}" "${!key}"
				# set context correctly
				sh_util::log 2 "$msg" "${FUNCNAME[1]:-$0}"
				SH_UTIL_DEBUG_vars[$key]="${!key}"
			fi
		done
		;;
	esac
}
# }}}
