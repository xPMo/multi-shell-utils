#!/usr/bin/env bash

#shellcheck disable=1090
source ../bin/*.bash

#shellcheck disable=2034
declare -A actual
sh_util::args setup -i 'S:count actual[c]' +i 'S:count actual[c]' \
	-t 'S:toggle actual[t1]' --toggle 'S:toggle actual[t1]' \
	-T 'S:toggle actual[t2]' +T 'S:toggle actual[t2]' \
	-v 'S:value actual[v]' --value 'S:value actual[v]' \
	-A 'S:append actual_a' --add 'S:append actual_a' \
	--add2 'S:append actual_a 2' \
	-E 'S:list actual_l ,'

# For each flag type, add an argument after
# the flag and its arguments
sh_util::args parse -i arg1 -ii +i \
	--toggle arg2 -T +TT \
	-v bad arg3 -vgood \
	--add=foo arg4 \
	--add2 bar baz arg5 \
	-E l1 l2 l3 , arg6

#shellcheck disable=2034
declare -A verify=([c]=2 [t1]=1 [t2]=0 [v]=good)
#shellcheck disable=2034
declare -a verify_a=(foo bar baz)
#shellcheck disable=2034
declare -a verify_l=(l{1..3})
#shellcheck disable=2034
declare -a args_verify=(arg{1..6})

# [[ Verify results ]]

verify(){
	local -n _actual="$1"
	local -n _verify="$2"
	if (( ${#_actual[@]} != ${#_verify[@]} )); then
		echo "Test failed: $1 != $2"
		echo "   ${_actual[*]@Q} != ${_verify[*]@Q}"
		exit 1
	fi
	for key in "${!_actual[@]}"; do
		if [[ "${_actual[$key]}" != "${_verify[$key]}" ]]; then
			echo "Test failed: $1[$key] != $2[$key]"
			echo "   ${_actual[$key]} != ${_verify[$key]}"
			exit 1
		fi
	done
}

verify SH_UTIL_args args_verify
verify actual verify
verify actual_a verify_a
verify actual_l verify_l

echo 'Test passed'
